var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var twitterSchema = new Schema({
    screen_name: String,
    name: String,
    registered_at: Date,
    description: String,
    total_tweets: Number,
    following: Number,
    followers: Number,
    favourite_posts: Number,
    timezone: String,
    location: String,
    last_tweet: String
});

module.exports = mongoose.model('Twitter', twitterSchema);