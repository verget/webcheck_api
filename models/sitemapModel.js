var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sitemapSchema = new Schema({
    websiteId: String,
    siteMap: [Schema.Types.Mixed],
    scanStart: Date,
    scanEnd: Date,
    lastCheck: Date
});

module.exports = mongoose.model('Sitemap', sitemapSchema);