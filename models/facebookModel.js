var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var facebookSchema = new Schema({
    username: String,
    name: String,
    gender: String,
    location: String
});

module.exports = mongoose.model('Facebook', facebookSchema);