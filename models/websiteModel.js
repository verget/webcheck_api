var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var websiteSchema = new Schema({
    id: 'String',
    title: 'String',
    responseCode: 'Number',
    loadTime: 'Number',
    pageStats: {},
    version: {},
    ruleGroups: {},
    url: {
        protocol: String,
        slashes: false,
        auth: '',
        host: '',
        port: '',
        hostname: '',
        hash: '',
        search: "",
        query: {},
        pathname: '',
        path: '',
        href: ''
    },
    attributes: {
        title: String,
        meta: [],
        html: String,
        textHeaders: {},
        responseHeaders: {},
    },
    lastCheck: Date
});

module.exports = mongoose.model('Website', websiteSchema);