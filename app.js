var config = require('./config.js');
var express = require('express');
var expressSession = require('express-session');
var http = require('http');
var path = require('path');
var passport = require('passport');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

app.all("*", function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
    return next();
});

app.use(express.static(path.join(__dirname, '/public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressSession( {
        secret: config.server.session_key,
        resave: false,
        saveUninitialized: true
    }
));
app.use(passport.initialize());
app.use(passport.session());
app.use(logger('dev'));

require('./controllers/passportController')(passport);

var routes = require('./routes/index')(passport);
app.use('/', routes);

var mongoose = require('mongoose');

mongoose.connect(config.server.mongodb);


app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send({'success': false, message: err.message, error: err});
    });
} else {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send({'success': false, message: err.message});
    });
}

http.createServer(app).listen(config.server.port, function () {
    if ('development' == config.server.env) {
        console.log('Api server listening on port ' + config.server.port);
    }
});
