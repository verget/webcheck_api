var bCrypt = require('bcrypt-nodejs');

var User = require('../models/userModel');

var _this = this;

_this.createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};


module.exports = function(data, done){
    process.nextTick(function() {
        User.findOne({'username': data.username}, function (err, user) {
            if (err) {
                console.log('Error in SignUp: ' + err);
                return done({status: 403, message: err});
            }
            if (user) {
                console.log('User already exists');
                return done({status: 403, message: 'User already exists'});
            } else {
                var newUser = new User();
                newUser.username = data.username;
                newUser.password = _this.createHash(data.password);
                newUser.email = data.email;
                newUser.firstName = data.firstName;
                newUser.lastName = data.lastName;
                newUser.birthday = data.birthday;

                newUser.save(function (err) {
                    if (err) {
                        console.log('Error while user saving' + err);
                        throw err;
                    }
                    console.log('User Registration succesful');
                    return done(null, newUser);
                });
            }
        });
    });
};