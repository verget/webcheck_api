var request = require('request');
var urlUtil = require('url');
var config = require('../config.js');
var ApiRequest = require('../models/apiRequestModel');
var siteMap = require('../models/sitemapModel');
var webSite = require('../models/websiteModel');
var twitterModel = require('../models/twitterModel');
var facebookModel = require('../models/facebookModel');
var _this = this;


_this.isUrl = function(urlString) {
    var url = urlUtil.parse(urlString);
    if( !url ) {
        return false;
    }
    if( url.protocol && url.hostname ) {
        return url;
    }
    return false;
};

_this.crawlerApiCall = function(url, testsArr, callback) {

    var options = {
        url: config.crawler.host + ":" + config.crawler.port + config.crawler.route,
        method: 'POST',
        body: {},
        json: true
    };

    var tests = [];

    testsArr.forEach(function(item) {
        var tmpObj = {};
        tmpObj.type = item;
        tmpObj.id = '';
        tmpObj.status= 'sent';
        tests.push(tmpObj);
    });

    var apiRequest = new ApiRequest();
    apiRequest.username = ''; //todo
    apiRequest.url = url;
    apiRequest.tests = tests;
    apiRequest.status = 'sent';
    apiRequest.save(function(error){
        if (error){
            console.log(error);
        }else{
            options.body.requestId = apiRequest._id;
            request(options, function(error, response, body){
                if (!error && response.statusCode == 200) {
                    callback(null, body);
                }else{
                    callback(error);
                }
            });
        }
    });

};

_this.saveAnswer = function (responce, callback) {
    ApiRequest.findOne({_id : responce.requestId}, function(err, request){
        if (!err){
            request.status = 'returned';
            request.save();
            callback(null);
        }else{
            callback(err);
        }
    })
};
_this.getRequestStatus = function (id, callback){
    ApiRequest.findOne({_id : id}, function(err, request){
        if (err){
            callback(err, null);
        }else{
            callback(null, request.status);
        }
    });
};

_this.getRequestResult = function (id, callback){
    ApiRequest.findOne({_id : id}, function(err, request){
        if (err){
            callback(err);
        }else{
            callback(null, request.tests);
        }
    });
};

_this.getTestResult = function (testName, testId, callback){

    switch (testName) {
        case 'siteMap':
            siteMap.findById(testId, function(err, result){
                if (!err) {
                    callback(null, result);
                }
            });
            break;
        case 'info':
            webSite.findById(testId, function(err, result){
                if (!err) {
                    callback(null, result);
                }
            });
            break;
        case 'summary': //todo model
            siteMap.findById(testId, function(err, result){
                if (!err) {
                    callback(null, result);
                }
            });
            break;
        case 'facebook':
            facebookModel.findById(testId, function(err, result){
                if (!err) {
                    callback(null, result);
                }
            });
            break;
        case 'twitter':
            twitterModel.findById(testId, function(err, result){
                if (!err) {
                    callback(null, result);
                }
            });
            break;
        case 'linkedin': //todo model
            siteMap.findById(testId, function(err, result){
                if (!err) {
                    callback(null, result);
                }
            });
            break;
    }
}

module.exports = _this;