
var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');

var User = require('../models/userModel');

var _this = this;

_this.isValidPassword = function(user, password){
    return bCrypt.compareSync(password, user.password);
};

module.exports = function (passport) {

    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {
        try {
            User.findById(id, function(err, user) {
                done(err, user);
            });
        } catch (e) {
            done(err)
        }
    });

    passport.use('login', new LocalStrategy({
            passReqToCallback : true
        },
        function (req, username, password, done) {
            User.findOne({'username' : username },
                function(err, user) {
                    if (err) {
                        return done(err);
                    }
                    if (!user || !_this.isValidPassword(user, password)){
                        console.log( "User " + username + " Not Found" );
                        return done({status: 401, message: 'Invalid Username or Password'});
                    }
                    return done (null, user);
                }
            );
        })
    );

};
