var express = require('express');
var router = express.Router();
var signupController = require('../controllers/signupController');
var crawlerController = require('../controllers/crawlerController');
//var ApiRequest = require('../models/apiRequestModel');

var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
};

module.exports = function(passport){
    router.get('/', function(req, res) {
        res.sendFile('index.html', { message: req.flash('message')});
    });

    router.post('/login', passport.authenticate('login'), function(req, res){
            var user = req.user.toObject();
            delete user.password;
            res.send(user);
        }
    );

    router.post('/signup', function(req, res){
        signupController(req.body, function(error, response){
            if (error){
                res.send(error);
            }else{
                var user = response.toObject();
                delete user.password;
                res.send(user);
            }
        });
    });

    router.get('/signout', function (req, res) {
        req.session.destroy();
        req.logout();
    });

    router.post('/crawlerAnswer', function(req, res){ //todo fix 404 answer
        crawlerController.saveAnswer(req.body, function(error){
           if (!error){
               res.send('success');
               console.log('success');
           }
        });
    });

    router.get('/sendRequest', function(req, res){
        if( !req.query || !req.query['url'] ) {
            return res.sendStatus(400);
        }
        var url = crawlerController.isUrl(req.query['url']);
        if( !url ) {
            return res.status(400);
        }

        var testsStr = req.query.tests;
        var testsArr = testsStr.split(' ');
        crawlerController.crawlerApiCall(url.href, testsArr, function(error, data){
            if (!error){
                res.send(data);
            }else {
                console.log(error);
            }
        });
    });

    router.get('/getRequestStatus', function(req, res){
        crawlerController.getRequestStatus(req.query.id, function(error, data){
            if(!error){
                res.send(data);
            }
        });
    });

    router.get('/getRequestResult', function(req, res){
        crawlerController.getRequestResult(req.query.id, function(error, data){
            if(!error){
                res.send(data);
            }
        });
    });

    router.get('/getTestResult', function(req, res){
        crawlerController.getTestResult(req.query.test, req.query.id, function(error, data){
            if(!error){
                res.send(data);
            }
        });
    });


    //router.get('/auth/fb',
    //    passport.authenticate('facebook', {
    //        scope: 'read_stream'
    //    })
    //);
    //
    //router.get('/auth/fb/callback',
    //    passport.authenticate('facebook', {
    //        successRedirect: '/',
    //        failureRedirect: '/auth' })
    //);
    //
    //router.get('/auth/vk',
    //    passport.authenticate('vk', {
    //        scope: ['friends']
    //    }),
    //    function (req, res) {
    //        // The request will be redirected to vk.com for authentication, so
    //        // this function will not be called.
    //    }
    //);
    //
    //
    //router.get('/auth/vk/callback', function(req, res, next){
    //        req.query.error ? res.redirect('/') : next();
    //    },
    //    passport.authenticate('vkontakte',
    //        { failureRedirect: '/' }), function (req, res) {
    //        res.redirect('/');
    //    }
    //);
    return router;
}